//ARRAY METHODS

//1. Mutator Methods - functions that mutate or change an array after they're created. These methods manipulat the original array performing various tasks such as adding and removing elements.

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

/*
	push() - add an element in the end of an array AND returns the array's length

	Syntax:
		arrayName.push(elements);
*/

console.log(`Current array: \n ${fruits}`);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log(`Mutated array from push method: \n ${fruits}`);

fruits.push("Avocado", "Guava");
console.log(`Mutated array from push method: \n ${fruits}`);

/*
	pop() - remove an element in the end of an array AND returns the removed element

	Syntax:
		arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(`Mutated array from pop method: \n ${fruits}`);

/*
	unshift() - adds one or more elements at the beginning of an array AND returns the array's length

	Syntax:
		arrayName.unshift(elements);
*/

fruitsLength = fruits.unshift("Lime", "Banana");
console.log(fruitsLength);
console.log(`Mutated array from unshift method: \n ${fruits}`);

/*
	shift() - remove an element at the beginning of an array AND returns the removed element

	Syntax:
		arrayName.shift();
*/

removedFruit = fruits.shift();
console.log(removedFruit);
console.log(`Mutated array from shift method: \n ${fruits}`);

/*
	splice() - simultaneously remove elements from a specified index and adds an element, AND returns the removed element

	Syntax:
		arrayName.splic(startingIndex, deleteCount, elementsToBeAdded);
*/

removedFruit = fruits.splice(1, 2, "Lime", "Cherry");
console.log(removedFruit);
console.log(`Mutated array from splice method: \n ${fruits}`);

/*
	sort() - rearranges the array elements in alphanumeric order (ascending). Character-based ordering, not Value-based.

	Syntax:
		arrayName.sort();
*/

fruits.sort();
console.log(`Mutated array from sort method: \n ${fruits}`);

/*
Sorting items in descending order

fruits.sort().reverse();
console.log(`Mutated array from sort then reverse method: \n ${fruits}`);
*/

/*
	reverse() - reverses the order of array elements

	Syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log(`Mutated array from reverse method: \n ${fruits}`);

//2. Non-mutator Methods - functions that do not modify or change an array after they are created. These methods do not manipulate the originals array performing various tasks such as returning element from an array and combining arrays and printing the output.

let countries = ["US", "PH", "CA", "SG", "TH", "PH", "FR", "DE"];

/*
	indexOf() - returns the index number of the first matching element found in an array. If no match was found, the result will be -1. The search process will be done from the first element proceeding to the last element

	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, indexStart);
*/

let firstIndex = countries.indexOf("PH");
console.log(`Result of indexOf method: \n ${firstIndex}`);

firstIndex = countries.indexOf("PH", 4);
console.log(`Result of indexOf method: \n ${firstIndex}`);

/*
	lastIndexOf() - returns the index number of the last matching element found in an array. If no match was found, the result will be -1. The search process will be done from the last element proceeding to the first element

	Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, indexStart);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log(`Result of lastIndexOf method: \n ${lastIndex}`);

lastIndex = countries.lastIndexOf("PH", 4);
console.log(`Result of lastIndexOf method: \n ${lastIndex}`);

/*
	slice() - portions/slices elements from an array AND returns a new array

	Syntax:
		arrayName.slice(startIndex);
		arrayName.slice(startIndex, endIndex); //included start, excluded end
*/

let slicedArrayA = countries.slice(2);
console.log(`Result of slice method: \n ${slicedArrayA}`);
console.log(`Original array: \n ${countries}`);

let slicedArrayB = countries.slice(2,4);
console.log(`Result of slice method: \n ${slicedArrayB}`);
console.log(`Original array: \n ${countries}`);

let slicedArrayC = countries.slice(-3);
console.log(`Result of slice method: \n ${slicedArrayC}`);
console.log(`Original array: \n ${countries}`);

/*
	toString() - returns an array as a string separated by commas

	Syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log(`Result of toString method:`);
console.log(stringArray);

/*
	concat() - combines two arrays and returns the combined results

	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(element);
*/

let concatArray = slicedArrayA.concat(slicedArrayB);
console.log(`Result of concat method: \n ${concatArray}`);

concatArray = slicedArrayA.concat(slicedArrayB,slicedArrayC);
console.log(`Result of concat method: \n ${concatArray}`);

concatArray = concatArray.concat("CZ");
console.log(`Result of concat method: \n ${concatArray}`);

/*
	join() - returns an array as a string separated by specified separator string

	Syntax:
		arrayName.join(separatorString);
*/

let separatedArray = countries.join();
console.log(`Result of join method: \n ${separatedArray}`);

separatedArray = countries.join('');
console.log(`Result of join method: \n ${separatedArray}`);

separatedArray = countries.join(' | ');
console.log(`Result of join method: \n ${separatedArray}`);

//3. Iteration Methods - loops designed to perform repetitive tasks on arrays. Useful for manipulating array data resulting in complex tasks

/*
	forEach() - similar to a for loop that iterates on each array element

	Syntax:
		arrayName.forEach(function(individualElement) {
			statement
		})
*/

countries.forEach(function (country) {
	console.log(country);
});

let filteredCountries = [];

countries.forEach(function (country) {
	if (country.includes("H")) {
		filteredCountries.push(country);
	}
});

console.log(`Result of forEach method: \n ${filteredCountries}`);

/*
	map() - iterates on each element AND returns the new array with different values depending on the result of the function's operation

	Syntax:
		let/const resultArray = arrayName.map(function(individualElement) {
			statement
		});
*/

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function (number) {
	return number ** 2;
});

console.log(`Original array: \n ${numbers}`);
console.log(`Result of map method: \n ${numberMap}`);

/*
	every() - checks if all elements in an array satisfies the given condition

	Syntax:
		let/const resultArray = arrayName.every(function (individualElement) {
			return condition;
		});
*/

let allValid = numbers.every(function (number) {
	return number < 3;
});

console.log(`Result of every method: \n ${allValid}`);

/*
	some() - checks if at least one element in an array satisfies the given condition

	Syntax:
		let/const resultArray = arrayName.some(function (individualElement) {
			return condition;
		});
*/

let someValid = numbers.some(function (number) {
	return number < 3;
});

console.log(`Result of some method: \n ${someValid}`);

/*
	filter() - returns a new array that contains elements which satisfies the given condition

	Syntax:
		let/const resultArray = arrayName.filter(function(individualElement) {
			return condition;	
		});
*/

let filterValid = numbers.filter(function (number) {
	return number < 3;
});

console.log(`Result of filter method: \n ${filterValid}`);

filterValid = numbers.filter(function (number) {
	return number > 5;
});

console.log(`Result of filter method: \n ${filterValid}`);

//Filtering elements using forEach

let filteredNumbers = [];

numbers.forEach(function (number) {
	if (number < 3) {
		filteredNumbers.push(number);
	};
});

console.log(`Result of filtering using forEach method: \n ${filteredNumbers}`);

/*
	includes() - checks if an array contains a specified value
	
	Syntax:
		arrayName.includes(element);
		arrayName.includes(element, startIndex);
*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let includesProduct = products.includes("Keyboard");
console.log(`Result of includes method: \n ${includesProduct}`);

let filteredProducts = products.filter(function (product) {
	return product.toLowerCase().includes("a");
});

console.log(`Result of filter and includes method: \n ${filteredProducts}`);